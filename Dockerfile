FROM gitpod/workspace-full as tmp

USER root

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

FROM tmp

USER gitpod
